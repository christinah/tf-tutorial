import tensorflow as tf
import numpy as np
import os

from tensorflow.examples.tutorials.mnist import input_data
from utils import *
from scipy.misc import imsave as ims
from scipy.misc import toimage

# load mnist data
mnist = input_data.read_data_sets('MNIST_data')
mnist_train_images = mnist.train.images
mnist_test_images = mnist.test.images

# total number of features
d_data = 784
# images dimensions
d_img = 28
# dimension of latent features
n_z = 30
# dimensions of layers in nonlinear model
d_h = 500
# batch size for optimization
batch_size = 100
# directory to save visualizations
save_dir = "ae_out/"
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)

model = "linear" # other options: "nonlinear", "conv"

# Construct Graph
# name the input placeholder x; it should be a matrix of size batch_size x d_data
x = tf.placeholder(tf.float32, [None, d_data], name="input")

if model == "linear":
    # Task 1: x -> z -> y
    # use a linear model for x -> z and z -> y
    # i.e. both the encoder and the decoder have a weight matrix W and a bias vector b
    # the dimension of z is determined by n_z
    # Initialize the weights W_enc and W_dec with tf.random_uniform()
    # the output should be called y

    # Hidden Layer Variables
    W_enc = tf.Variable(tf.random_uniform([d_data, n_z], -1, 1), name="W_enc")
    b_enc = tf.Variable(tf.zeros([n_z]), name = "bias_enc")
    W_dec = tf.Variable(tf.random_uniform([n_z, d_data], -1, 1), name="W_dec")
    b_dec = tf.Variable(tf.zeros([d_data]), name = "bias_dec")

    # Hidden layer graph
    z = (tf.matmul(x, W_enc) + b_enc)

    # Output and reconstruction loss
    y = (tf.matmul(z, W_dec) + b_dec)

elif model == "nonlinear":

    # Task 2: x -> e1 -> e2 -> z -> d1 -> d2 -> y
    # use nonlinear layers for x -> e1 -> e2 -> z and z -> d1 -> d2 -> y
    # use tf.layers.dense() to create the layers
    # for e1, e2, d1 and d2 choose a nonlinear activation function
    # choose an initializer for 'kernel_initializer' in all calls of tf.layers.dense()
    # the dimensions for e1, e2, d1 and d2 are determined by d_h
    # the dimension of z is determined by n_z
    # the output should be called y

    e1 = tf.layers.dense(x,
                         units = d_h,
                         activation=tf.nn.softplus,
                         kernel_initializer=tf.contrib.layers.xavier_initializer(),
                         bias_initializer=tf.zeros_initializer(),
                         name = "h1_rec")

    e2 = tf.layers.dense(e1,
                         units = d_h,
                         activation=tf.nn.softplus,
                         kernel_initializer=tf.contrib.layers.xavier_initializer(),
                         bias_initializer=tf.zeros_initializer(),
                         name = "h2_rec")

    z = tf.layers.dense(e2,
                        units = n_z,
                        kernel_initializer=tf.contrib.layers.xavier_initializer(),
                        bias_initializer=tf.zeros_initializer(),
                        name = "z")

    d1 = tf.layers.dense(z,
                         units = d_h,
                         activation=tf.nn.softplus,
                         kernel_initializer=tf.contrib.layers.xavier_initializer(),
                         name = "h1_gen")

    d2 = tf.layers.dense(d1,
                         units = d_h,
                         activation=tf.nn.softplus,
                         kernel_initializer=tf.contrib.layers.xavier_initializer(),
                         name = "h2_gen")

    y = tf.layers.dense(d2,
                        units = d_data,
                        name = "y")
elif model == "conv":
    # Task 3: x -> -> z -> y
    # use convolutional layers for x -> z and z -> -> y
    # use tf.layers.conv2d() to create the layers in the encoder
    # use tf.layers.conv2d_transpose() to create the layers in the decoder
    # choose a nonlinear activation function for the convolutional layers,
    # and tf.nn.sigmoid for the last one (mapping to y)
    # the dimension of z is determined by n_z
    # the output should be called y
    images_matrix = tf.reshape(x,[-1, d_img, d_img, 1])
    # fill the arguments to map the input image of size 28x28x1 to a tensor of size 14x14x16
    # use a 5x5 kernel
    e1 = tf.layers.conv2d(images_matrix,
                          filters = 16,
                          kernel_size=[5, 5],
                          strides=2,
                          padding="same",
                          activation=tf.nn.relu,
                          name = "rec_conv_h1") # 28x28x1 -> 14x14x16
    # fill the arguments to map the input of size 14x14x16 to a tensor of size 7x7x32
    # use a 5x5 kernel
    e2 = tf.layers.conv2d(e1,
                          filters = 32,
                          kernel_size=[5, 5],
                          strides=2,
                          padding="same",
                          activation=tf.nn.relu,
                          name = "rec_conv_h2") # 14x14x16 -> 7x7x32
    # reshape 7x7x32 tensor to a vector
    e2_flat = tf.reshape(e2,[-1, 7*7*32])
    # use tf.layers.dense() to map to z
    z = tf.layers.dense(e2_flat, units = n_z, name = "z")
    # use tf.layers.dense() to map to 7*7*32 units
    d1 = tf.layers.dense(z, units = 7*7*32)
    # reshape and apply a nonlinearity
    d1_mat = tf.nn.relu(tf.reshape(d1, [-1, 7, 7, 32]))
    d2 = tf.layers.conv2d_transpose(d1_mat,
                                    kernel_size=[5, 5],
                                    filters = 16,
                                    strides=2,
                                    padding="same",
                                    activation=tf.nn.relu,
                                    name = "g_h1")
    # for the activation function use a sigmoid
    y_img = tf.layers.conv2d_transpose(d2,
                                    kernel_size=[5, 5],
                                    filters = 1,
                                    strides=2,
                                    padding="same",
                                    activation=tf.nn.sigmoid,
                                    name = "g_h2")
    # reshape to a vector
    y = tf.reshape(y_img, [-1, d_data])
else:
    raise NotImplementedError

loss = tf.sqrt(tf.reduce_sum(tf.square(x - y)))

# Optimizer
opt = tf.train.GradientDescentOptimizer(0.01)
update_step = opt.minimize(loss)

gen_train_batch = batch_generator([mnist_train_images], batch_size)
gen_test_batch = batch_generator([mnist_test_images], batch_size)

 # Train
with tf.Session() as session:
 # Initialize variables
 init = tf.global_variables_initializer()
 session.run(init)
 # Do nsteps SGD update steps
 for i in range(10000):
     X = next(gen_train_batch)
     X = X[0].reshape([-1, d_data])
     feed_dict = {x : X}
     session.run(update_step, feed_dict = feed_dict)
     # Every 200 iterations visualize reconstructions on training and test set
     if i % 200 == 0:
         gen_img_tr = session.run(y, feed_dict = feed_dict)
         gen_img_tr = gen_img_tr.reshape([-1, d_img, d_img])
         num_imgs = int(np.floor(np.sqrt(gen_img_tr.shape[0])))
         im = toimage(merge(gen_img_tr[:(num_imgs*num_imgs)],[num_imgs,num_imgs]))
         im.save(os.path.join(save_dir,str(i)+"_tr.jpg"))

         X = next(gen_test_batch)
         X = X[0].reshape([-1, d_data])
         feed_dict = {x : X}
         test_loss, gen_img_te = session.run([loss, y], feed_dict = feed_dict)
         print("test loss is %f " % test_loss)
         gen_img_te = gen_img_te.reshape([-1, d_img, d_img])
         im = toimage(merge(gen_img_te[:(num_imgs*num_imgs)],[num_imgs,num_imgs]))
         im.save(os.path.join(save_dir,str(i)+"_te.jpg"))

# TensorFlow Workshop SfS

## Installation

### Locally
If you want to install TensorFlow locally, follow the instructions given [here](https://www.tensorflow.org/install/). The virtualenv installation is recommended.

### Ada's
On the ada machines, TensorFlow is installed in a Python virtual environment. The details can be found [here](https://blogs.ethz.ch/isgdmath/tensorflow/).

## Slides

## Exercises

Some exercises can be found in the 'exercises/' folder.

* Simple linear regression: ``linear_regression_template.py``
* Autoencoder (linear, nonlinear, convolutional): ``ae_template.py``
